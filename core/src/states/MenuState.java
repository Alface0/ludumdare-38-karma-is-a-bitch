package states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by drmargarido on 22-04-2017.
 */
public class MenuState extends State {
    public MenuState(GameStateManager gameStateManager) {
        super(gameStateManager);
    }

    @Override
    public void update(float deltaTime) {

    }

    @Override
    public void render(SpriteBatch spriteBatch) {

    }

    @Override
    public void dispose() {

    }

    @Override
    public void handleInput() {

    }
}
