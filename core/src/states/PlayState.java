package states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Intersector;
import com.mygdx.ld38.Constants;
import gameobjects.*;

import java.util.List;

/**
 * Created by drmargarido on 22-04-2017.
 */
public class PlayState extends State {
    private World smallWorld;
    private Player player;
    private ObstacleManager obstacleManager;
    private float delayTimePassed;

    private float score;
    private String scoreName;
    private BitmapFont bitmapFontName;

    private String gameOverText;
    private BitmapFont gameOverBitmapFont;

    public PlayState(GameStateManager gameStateManager) {
        super(gameStateManager);

        // Creating world
        smallWorld = new World(Constants.WORLD_RADIUS, "world.png");
        player = new Player();
        obstacleManager = new ObstacleManager();
        delayTimePassed = 0;
        score = 0;
        scoreName = "score: 0";
        bitmapFontName = new BitmapFont();

        gameOverText = "Game Over: Press R to restart!";
        gameOverBitmapFont = new BitmapFont();
    }

    @Override
    public void update(float deltaTime) {
        if(!player.isAlive())
        {
            delayTimePassed += deltaTime;
        }
        else {

            smallWorld.update(deltaTime);
            player.update(deltaTime);

            // Player Collision with obstacles
            List<SpinningObject> playerCollisions = obstacleManager.detectObstacleCollisionWith(player.mesh);
            if (playerCollisions.size() > 0) {
                // Detected collisions with the spinning Object
                for (SpinningObject spinningObject : playerCollisions) {
                    if (spinningObject instanceof Platform) {
                        if (player.distanceFromGround >= spinningObject.distanceFromGround) {
                            player.setJumpSpeed(0);
                            player.setCollided();
                        } else {
                            player.kill();
                        }
                    } else if (spinningObject instanceof Alien) {
                        if (((Alien) spinningObject).isAlive())
                            player.kill();
                    }
                }
            }

            if (!player.hasAxe()) {
                // Axe collision with player
                if (Intersector.overlapConvexPolygons(player.getAxe().mesh, player.mesh)) {
                    player.kill();
                    player.setRotationSpeed(0);
                    player.getAxe().dispose();
                    player.setHasAxe(true);

                    obstacleManager.stopWorld();
                }

                // Axe Collision with obstacles
                List<SpinningObject> axeCollisions = obstacleManager.detectObstacleCollisionWith(player.getAxe().mesh);
                for (SpinningObject spinningObject : axeCollisions) {
                    if (spinningObject instanceof Alien) {
                        if (((Alien) spinningObject).isAlive()) {
                            player.getAxe().dispose();
                            player.setHasAxe(true);
                            spinningObject.setRotationSpeed(0);
                            ((Alien) spinningObject).kill();
                            score += Constants.SCORE_POINTS_BY_ALIEN_KILL;

                            obstacleManager.destroyObstacle(spinningObject, Constants.TIME_TO_ALIEN_DISAPPEAR);
                            break;
                        }
                    }
                }
            }

            obstacleManager.update(deltaTime);
            score += Constants.SCORE_POINTS_BY_SECOND * deltaTime;
            scoreName = "Score: " + (int) score;
        }
    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        smallWorld.render(spriteBatch);
        obstacleManager.render(spriteBatch);
        player.render(spriteBatch);
        bitmapFontName.setColor(1.0f, 1.0f, 1.0f, 1.0f);
        bitmapFontName.draw(spriteBatch, scoreName, 20, Constants.HEIGHT - 20);

        if(!player.isAlive())
        {
            gameOverBitmapFont.setColor(1.0f, 1.0f, 1.0f, 1.0f);
            gameOverBitmapFont.draw(spriteBatch, gameOverText,
                    Constants.WIDTH / 2 - 100, Constants.HEIGHT - Constants.HEIGHT / 4);
        }
    }

    @Override
    public void dispose() {
        smallWorld.dispose();
        player.dispose();
    }

    @Override
    public void handleInput() {
        if(Gdx.input.isKeyPressed(Input.Keys.SPACE)){
            player.jump();
        }
        if(Gdx.input.justTouched())
        {
            if(player.isAlive())
                player.throwAxe();
        }
        if(Gdx.input.isKeyPressed(Input.Keys.R))
        {
            if(delayTimePassed >= Constants.TIME_DELAY_TO_GAME_OVER)
                gameStateManager.setState(new PlayState(gameStateManager));
        }
    }
}
