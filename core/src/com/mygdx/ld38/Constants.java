package com.mygdx.ld38;

/**
 * Created by drmargarido on 22-04-2017.
 */
public class Constants {
    public static final int WIDTH = 800;
    public static final int HEIGHT = 640;
    public static final String TITLE = "Karma Is A Bitch";
    public static final int WORLD_RADIUS = 350;

    public static final float PLAYER_VELOCITY = 25;
    public static final float PLATFORMS_VELOCITY = 15;

    public static final float GRAVITY = -250;
    public static final float JUMP_SPEED = 160;

    public static final float PLATFORM_SPAWN_PROBABILITY = 0.6f;
    public static final float ALIEN_SPAWN_PROBABILITY = 0.15f;

    public static final float MIN_TIME_TO_NEXT_OBJECT = 1.0f; // seconds
    public static final float MAX_TIME_TO_NEXT_OBJECT = 5.0f; // seconds

    public static final float ALIENS_VELOCITY = 25;
    public static final float TIME_TO_ALIEN_DISAPPEAR = 0.5f; // Milliseconds

    public static final float AXE_VELOCITY = 60;

    public static final float TIME_DELAY_TO_GAME_OVER = 0.2f; // seconds

    public static final float SCORE_POINTS_BY_SECOND = 5;
    public static final float SCORE_POINTS_BY_ALIEN_KILL = 20;
}
