package gameobjects;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.mygdx.ld38.Constants;

/**
 * Created by drmargarido on 23-04-2017.
 */
public class Axe extends AnimatedSpinningObject {

    public Axe(float degrees, float distanceFromGround) {
        super(degrees, distanceFromGround, Constants.AXE_VELOCITY, "axe_sprite.png", 20, 20);
        TextureRegion[][] textureRegions = TextureRegion.split(texture, 256, 252);

        TextureRegion [] animationFrames = new TextureRegion[8];
        animationFrames[0] = textureRegions[0][0];
        animationFrames[1] = textureRegions[0][1];
        animationFrames[2] = textureRegions[0][2];
        animationFrames[3] = textureRegions[0][3];
        animationFrames[4] = textureRegions[0][4];
        animationFrames[5] = textureRegions[0][5];
        animationFrames[6] = textureRegions[0][6];
        animationFrames[7] = textureRegions[0][7];

        animation = new Animation<TextureRegion>(0.075f, animationFrames);
    }
}
