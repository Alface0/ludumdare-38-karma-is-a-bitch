package gameobjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.ld38.Constants;

import static com.badlogic.gdx.math.MathUtils.cos;
import static com.badlogic.gdx.math.MathUtils.sin;

/**
 * Created by drmargarido on 22-04-2017.
 */
public abstract class SpinningObject implements GameObject {
    protected int ROTATION_OFFSET = 270;
    protected float degrees;
    public float distanceFromGround;
    protected Vector2 position;

    protected float rotationSpeed;

    protected Texture texture;
    protected TextureRegion activeRegion;
    public Polygon mesh;

    protected int width;
    protected int height;

    public SpinningObject(float degrees, float distanceFromGround, float rotationSpeed, String textureName, int width, int height)
    {
        this.degrees = degrees;
        this.distanceFromGround = distanceFromGround;
        this.rotationSpeed = rotationSpeed;
        this.width = width;
        this.height = height;

        this.position = new Vector2(Constants.WIDTH / 2 + Constants.WORLD_RADIUS / 2 * cos(degrees * MathUtils.degreesToRadians)
                    + distanceFromGround * cos(degrees * MathUtils.degreesToRadians),
                Constants.HEIGHT / 2 + Constants.WORLD_RADIUS / 2 * sin(degrees * MathUtils.degreesToRadians)
                        + distanceFromGround * sin(degrees * MathUtils.degreesToRadians));

        mesh = new Polygon(new float[]{0, 0, width - 3, 0, width - 3, height - 6, 0, height - 6});
        mesh.setOrigin(0, 0);

        texture = new Texture(textureName);
        activeRegion = new TextureRegion(texture);
    }

    @Override
    public void update(float deltaTime) {
        degrees += rotationSpeed * deltaTime;

        position.set(Constants.WIDTH / 2 + Constants.WORLD_RADIUS / 2 * cos(degrees * MathUtils.degreesToRadians)
                        + distanceFromGround * cos(degrees * MathUtils.degreesToRadians),
                Constants.HEIGHT / 2 + Constants.WORLD_RADIUS / 2 * sin(degrees * MathUtils.degreesToRadians)
                        + distanceFromGround * sin(degrees * MathUtils.degreesToRadians));

        mesh.setPosition(position.x, position.y);
        mesh.setRotation(degrees + 270);
    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        spriteBatch.draw(activeRegion, position.x, position.y, 0,
                0, width, height, 1, 1, degrees + ROTATION_OFFSET);
    }

    @Override
    public void dispose() {
        texture.dispose();
    }

    public Vector2 getPosition()
    {
        return position;
    }

    public void setRotationSpeed(float rotationSpeed) {
        this.rotationSpeed = rotationSpeed;
    }
}
