package gameobjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.mygdx.ld38.Constants;

/**
 * Created by drmargarido on 22-04-2017.
 */
public class Player extends SpinningObject {
    private static int playerWIDTH = 25;
    private static int playerHEIGHT = 50;
    private static final float frameDuration = 0.15f;

    private float jumpSpeed;
    private Animation<TextureRegion> animation;

    private Texture noAxeTexture;
    private Animation<TextureRegion> noAxeAnimation;

    private float stateTime;

    private boolean collided;
    private float timeSinceCollision;

    private static final float timeToJump = 0.1f;
    private boolean hasAxe;
    private Axe axe;

    private boolean isAlive;
    private TextureRegion deathTexture;

    public Player()
    {
        super(25, 0, Constants.PLAYER_VELOCITY, "player_sprite.png", playerWIDTH, playerHEIGHT);
        jumpSpeed = 0;

        TextureRegion[][] textureRegions = TextureRegion.split(texture, 127, 221);

        TextureRegion [] animationFrames = new TextureRegion[4];
        animationFrames[0] = textureRegions[0][0];
        animationFrames[1] = textureRegions[0][1];
        animationFrames[2] = textureRegions[0][2];
        animationFrames[3] = textureRegions[0][1];

        animation = new Animation<TextureRegion>(frameDuration, animationFrames);

        noAxeTexture = new Texture("player_sprite_without_axe.png");
        TextureRegion[][] noAxeTextureRegions = TextureRegion.split(noAxeTexture, 127, 221);

        TextureRegion [] noAxeAnimationFrames = new TextureRegion[4];
        noAxeAnimationFrames[0] = noAxeTextureRegions[0][0];
        noAxeAnimationFrames[1] = noAxeTextureRegions[0][1];
        noAxeAnimationFrames[2] = noAxeTextureRegions[0][2];
        noAxeAnimationFrames[3] = noAxeTextureRegions[0][1];

        noAxeAnimation = new Animation<TextureRegion>(frameDuration, noAxeAnimationFrames);

        deathTexture = new TextureRegion(new Texture("player_death.png"));

        collided = false;
        hasAxe = true;
        isAlive = true;
    }

    public void throwAxe() {
        if(hasAxe) {
            hasAxe = false;
            axe = new Axe(degrees + 5, distanceFromGround + height / 3);
        }
    }

    public boolean isAlive()
    {
        return isAlive;
    }

    public boolean hasAxe()
    {
        return hasAxe;
    }

    public void setHasAxe(boolean hasAxe)
    {
        this.hasAxe = hasAxe;
    }

    public Axe getAxe()
    {
        return axe;
    }

    public void setCollided() {
        this.collided = true;
        timeSinceCollision = 0;
    }

    @Override
    public void update(float deltaTime) {
        stateTime += deltaTime;
        super.update(deltaTime);

        if(distanceFromGround > 0)
        {
            distanceFromGround += jumpSpeed * deltaTime;
            jumpSpeed += Constants.GRAVITY * deltaTime;
        }
        else
        {
            distanceFromGround = 0;
        }

        timeSinceCollision += deltaTime;

        if(timeSinceCollision >= timeToJump)
            collided = false;

        if(!hasAxe)
            axe.update(deltaTime);
    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        if(isAlive) {
            if (hasAxe)
                activeRegion = animation.getKeyFrame(stateTime, true);
            else
                activeRegion = noAxeAnimation.getKeyFrame(stateTime, true);
        }
        else
        {
            activeRegion = deathTexture;
        }

        super.render(spriteBatch);

        if(!hasAxe)
            axe.render(spriteBatch);
    }

    public void jump()
    {
        if(distanceFromGround <= 0 || collided)
        {
            jumpSpeed = Constants.JUMP_SPEED;
            distanceFromGround += 5f;
        }
    }

    public void setJumpSpeed(float jumpSpeed) {
        this.jumpSpeed = jumpSpeed;
    }

    @Override
    public void dispose() {
        super.dispose();

        if(!hasAxe)
            axe.dispose();

        noAxeTexture.dispose();
        deathTexture.getTexture().dispose();
    }

    public void kill() {
        isAlive = false;
    }
}
