package gameobjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.ld38.Constants;

import java.util.*;

/**
 * Created by drmargarido on 22-04-2017.
 */
public class ObstacleManager implements GameObject {
    private Vector2 spawnPoint;
    private int spawnPointRotation = 272;

    private int width = 40;
    private int height = 55;

    private Texture texture;
    private TextureRegion defaultTexture;
    private TextureRegion activeRegion;
    private Animation<TextureRegion> createAnimation;
    private Animation<TextureRegion> destroyAnimation;

    private Animation<TextureRegion> activeAnimation;
    private float stateTime;

    private List<SpinningObject> objectList;

    // Gap in degrees
    private float timeToNextObject;
    private float timeAfterObjectCreation;

    private float timeCounter;
    private List<Map.Entry<SpinningObject, Float>> objectsToDestroy;

    public ObstacleManager()
    {
        stateTime = 0;
        spawnPoint = new Vector2(Constants.WIDTH / 2 + Constants.WORLD_RADIUS / 2, Constants.HEIGHT / 2);
        texture = new Texture("spawn_sprite.png");

        TextureRegion[][] textureRegions = TextureRegion.split(texture, 345, 447);

        TextureRegion [] createAnimationFrames = new TextureRegion[3];
        createAnimationFrames[0] = textureRegions[0][1];
        createAnimationFrames[1] = textureRegions[0][2];
        createAnimationFrames[2] = textureRegions[0][0];
        createAnimation = new Animation<TextureRegion>(0.15f, createAnimationFrames);

        TextureRegion [] destroyAnimationFrames = new TextureRegion[3];
        destroyAnimationFrames[0] = textureRegions[0][2];
        destroyAnimationFrames[1] = textureRegions[0][1];
        destroyAnimationFrames[2] = textureRegions[0][0];
        destroyAnimation = new Animation<TextureRegion>(0.15f, destroyAnimationFrames);

        defaultTexture = textureRegions[0][0];
        activeAnimation = null;

        activeRegion = defaultTexture;

        objectList = new ArrayList<SpinningObject>();
        createObject();

        objectsToDestroy = new ArrayList<Map.Entry<SpinningObject, Float>>();
    }

    private void createObject()
    {
        Random random = new Random();
        float randomValue = random.nextFloat();

        if(randomValue >= 0 && randomValue < Constants.PLATFORM_SPAWN_PROBABILITY)
        {
            // Create Platform
            objectList.add(new Platform(spawnPointRotation + 91, 5 + random.nextFloat() * 30));
            triggerAnimation(createAnimation);
        }
        else if(randomValue >= Constants.PLATFORM_SPAWN_PROBABILITY
                && randomValue < Constants.PLATFORM_SPAWN_PROBABILITY + Constants.ALIEN_SPAWN_PROBABILITY)
        {
            // Create Alien
            objectList.add(new Alien(spawnPointRotation + 91));
            triggerAnimation(createAnimation);
        }
        else
        {
            // Do Nothing
        }

        timeToNextObject = (int) (Constants.MIN_TIME_TO_NEXT_OBJECT + random.nextFloat()
                * (Constants.MAX_TIME_TO_NEXT_OBJECT - Constants.MIN_TIME_TO_NEXT_OBJECT));
        timeAfterObjectCreation = 0;
    }


    private void triggerAnimation(Animation animation)
    {
        activeAnimation = animation;
        stateTime = 0;
    }

    public void update(float deltaTime)
    {
        timeAfterObjectCreation += deltaTime;
        stateTime += deltaTime;
        timeCounter += deltaTime;

        List<SpinningObject> temporaryObjectList = new ArrayList<SpinningObject>(objectList);
        for(SpinningObject spinningObject : temporaryObjectList) {
            spinningObject.update(deltaTime);

            if(spinningObject.degrees <= 10 + spinningObject.width / 8) {
                int indexOfObject = objectList.indexOf(spinningObject);
                destroyObject(objectList.get(indexOfObject));
            }
        }
        if(timeAfterObjectCreation > timeToNextObject)
            createObject();

        for(Map.Entry<SpinningObject, Float> spinningObject : objectsToDestroy)
        {
            if(spinningObject.getValue() < timeCounter)
            {
                objectList.remove(spinningObject.getKey());
                spinningObject.getKey().dispose();
            }
        }
    }

    // Destroy object when colliding with the spawn portal
    private void destroyObject(SpinningObject spinningObject) {
        spinningObject.dispose();
        objectList.remove(spinningObject);
        triggerAnimation(destroyAnimation);
    }

    public void render(SpriteBatch spriteBatch)
    {
        // Draw Objects
        for(SpinningObject spinningObject: objectList)
            spinningObject.render(spriteBatch);

        // Draw Spawn Point
        if(activeAnimation != null && !activeAnimation.isAnimationFinished(stateTime))
            activeRegion = activeAnimation.getKeyFrame(stateTime);
        else
            activeRegion = defaultTexture;

        spriteBatch.draw(activeRegion, spawnPoint.x - 1, spawnPoint.y + height / 2, 0, 0, width,
                height, 1, 1, spawnPointRotation);
    }

    @Override
    public void dispose() {
        texture.dispose();
    }

    public List<SpinningObject> detectObstacleCollisionWith(Polygon otherMesh)
    {
        List<SpinningObject> collidingObstacles = new ArrayList<SpinningObject>();

        for(SpinningObject spinningObject: objectList)
        {
            if(Intersector.overlapConvexPolygons(otherMesh, spinningObject.mesh))
                collidingObstacles.add(spinningObject);
        }

        return collidingObstacles;
    }

    // Destroy obstacle in some time from now
    public void destroyObstacle(SpinningObject spinningObject, float deltaTime)
    {
        objectsToDestroy.add(new AbstractMap.SimpleEntry<SpinningObject, Float>(spinningObject, timeCounter + deltaTime));
    }

    public void stopWorld()
    {
        for(SpinningObject spinningObject: objectList)
            spinningObject.setRotationSpeed(0);
    }
}
