package gameobjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.mygdx.ld38.Constants;

/**
 * Created by drmargarido on 22-04-2017.
 */
public class World implements GameObject {
    private Circle circle;
    private TextureRegion texture;

    public World(int radius, String textureName)
    {
        circle = new Circle(Constants.WIDTH / 2, Constants.HEIGHT / 2, radius);
        texture = new TextureRegion(new Texture(textureName));
    }

    @Override
    public void update(float deltaTime) {

    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        spriteBatch.draw(texture, circle.x - circle.radius / 2, circle.y - circle.radius / 2,
                circle.radius, circle.radius);
    }

    @Override
    public void dispose() {
       texture.getTexture().dispose();
    }
}
