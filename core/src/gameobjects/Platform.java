package gameobjects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.ld38.Constants;

/**
 * Created by drmargarido on 22-04-2017.
 */
public class Platform extends SpinningObject{

    public Platform(float degrees, float distanceFromGround) {
        super(degrees, distanceFromGround, Constants.PLATFORMS_VELOCITY, "platform.png", 60, 10);
        rotationSpeed = -rotationSpeed;
    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        spriteBatch.draw(activeRegion, position.x, position.y, 0,
                0, width, height, 1, 1, degrees + 260);
    }
}
