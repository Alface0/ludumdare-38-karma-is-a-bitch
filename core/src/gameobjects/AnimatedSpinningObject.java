package gameobjects;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by drmargarido on 23-04-2017.
 */
public class AnimatedSpinningObject extends SpinningObject {
    protected Animation<TextureRegion> animation;
    protected float stateTime;

    public AnimatedSpinningObject(float degrees, float distanceFromGround, float rotationSpeed, String textureName, int width, int height) {
        super(degrees, distanceFromGround, rotationSpeed, textureName, width, height);
    }

    @Override
    public void update(float deltaTime) {
        stateTime += deltaTime;
        super.update(deltaTime);
    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        activeRegion = animation.getKeyFrame(stateTime, true);
        super.render(spriteBatch);
    }
}
