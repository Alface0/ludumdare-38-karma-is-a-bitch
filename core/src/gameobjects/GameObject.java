package gameobjects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by drmargarido on 22-04-2017.
 */
public interface GameObject {
    public void update(float deltaTime);
    public void render(SpriteBatch spriteBatch);
    public void dispose();
}
