package gameobjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.mygdx.ld38.Constants;

/**
 * Created by drmargarido on 23-04-2017.
 */
public class Alien extends AnimatedSpinningObject {
    private boolean isAlive;
    private TextureRegion deathTexture;

    public Alien(float degrees) {
        super(degrees, 0, -Constants.ALIENS_VELOCITY, "alien_sprite.png", 45, 40);
        ROTATION_OFFSET = 265;

        TextureRegion[][] textureRegions = TextureRegion.split(texture, 425, 414);

        TextureRegion [] animationFrames = new TextureRegion[2];
        animationFrames[0] = textureRegions[0][0];
        animationFrames[1] = textureRegions[0][1];

        animation = new Animation<TextureRegion>(0.25f, animationFrames);
        isAlive = true;

        deathTexture = new TextureRegion(new Texture("alien_death.png"));
    }

    public boolean isAlive()
    {
        return isAlive;
    }

    public void kill() {
        isAlive = false;
    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        if(isAlive)
            super.render(spriteBatch);
        else
            spriteBatch.draw(deathTexture, position.x, position.y, 0,
                    0, width, height, 1, 1, degrees + ROTATION_OFFSET);
    }

    @Override
    public void dispose()
    {
        super.dispose();
        deathTexture.getTexture().dispose();
    }

}
